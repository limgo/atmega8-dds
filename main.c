#include <stdlib.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <math.h>
#include "lcd_lib.h"

PROGMEM  prog_uchar sine256[]  = {
  127,130,133,136,139,143,146,149,152,155,158,161,164,167,170,173,
  176,178,181,184,187,190,192,195,198,200,203,205,208,210,212,215,
  217,219,221,223,225,227,229,231,233,234,236,238,239,240,242,243,
  244,245,247,248,249,249,250,251,252,252,253,253,253,254,254,254,
  254,254,254,254,253,253,253,252,252,251,250,249,249,248,247,245,
  244,243,242,240,239,238,236,234,233,231,229,227,225,223,221,219,
  217,215,212,210,208,205,203,200,198,195,192,190,187,184,181,178,
  176,173,170,167,164,161,158,155,152,149,146,143,139,136,133,130,
  127,124,121,118,115,111,108,105,102, 99, 96, 93, 90, 87, 84, 81,
   78, 76, 73, 70, 67, 64, 62, 59, 56, 54, 51, 49, 46, 44, 42, 39,
   37, 35, 33, 31, 29, 27, 25, 23, 21, 20, 18, 16, 15, 14, 12, 11,
   10,  9,  7,  6,  5,  5,  4,  3,  2,  2,  1,  1,  1,  0,  0,  0,
    0,  0,  0,  0,  1,  1,  1,  2,  2,  3,  4,  5,  5,  6,  7,  9,
   10, 11, 12, 14, 15, 16, 18, 20, 21, 23, 25, 27, 29, 31, 33, 35,
   37, 39, 42, 44, 46, 49, 51, 54, 56, 59, 62, 64, 67, 70, 73, 76,
   78, 81, 84, 87, 90, 93, 96, 99,102,105,108,111,115,118,121,124
};

uint32_t tabHz[] = { 65,  69,  73,  78,  82,  87,  93,  98, 104, 110, 117, 123,
					131, 139, 147, 156, 165, 175, 185, 196, 208, 220, 233, 247,
					262, 277, 294, 311, 330, 349, 370, 392, 415, 440, 466, 494,
					523, 554, 587, 622, 659, 699, 740, 784, 813, 880, 932, 988 };

char* tabNote[] = {"C  ", "cis", "D  ", "dis", "E  ", "F  ", "fis", "G  ", "gis", "A  ", "ais", "H  " };
char* tabBetweenNote[] = {"C..cis", "cis..D", "D..dis", "dis..E", "E..F  ", "F..fis", "fis..G", "G..gis", "gis..A", "A..ais", "ais..H", "H..C  " };

#define ITOABUFSIZE	7
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#define encoder0PinA  2
#define encoder0PinB  3
#define MAX_TAB_HZ 	  48

char buffer[ITOABUFSIZE];

double dfreq;
const double refclk = 31376.6;

volatile char icnt;
volatile unsigned long phaccu;
volatile unsigned long tword_m;

uint32_t j = 0, t = 0;
uint32_t diff0 = 0, diff1 = 0;

uint32_t binSearch(uint32_t t[], uint32_t val, uint32_t length){

    uint32_t p = 0, k = length, c = 0;
    while(p!=k){
        c = (p+k)/2;
        if(c == 0 && t[c]>val)return 0;
        if((t[c]>val && t[c-1]<val) ||
            t[c]==val){
                return c;
        }
        else{
            if( val < t[c] )
                k = c - 1;
            else
                p = c + 1;
        }
    }
    return k;
}

void SetupPWM(void)
{
	DDRB = _BV(1);
	TCCR1B |= _BV(CS10);
	TCCR1A |= _BV(COM1A1) | _BV(WGM10);
	TIMSK  |= _BV(TOIE1);
	dfreq = 1000.0;
	tword_m = pow(2,32) * dfreq / refclk;
}

void SetupADC(void)
{
	ADMUX |= _BV(REFS0) | 0x05;
	ADCSRA = _BV(ADEN) | _BV(ADPS2);
}

uint16_t ReadADC(void)
{
	ADCSRA |= _BV(ADSC);
	while (ADCSRA & _BV(ADSC) ) {}
	return ADCW;
}

int main(void)
{
        uint16_t adc_val = 0;
	for(volatile uint16_t i=0; i<15000; i++);

	SetupPWM();
	SetupADC();
	sei();

	LCDinit();
	LCDclr();
	LCDvisible();

	while(1) {

		adc_val = ReadADC();
		adc_val += 16;
		if(adc_val > 1000)adc_val = 1000;

		tword_m = pow(2,32) * adc_val / refclk;

		LCDGotoXY(0,0);
		LCDstring("Freq: ", 6);

		itoa(adc_val, buffer, 10);
		LCDstring(buffer, 6);
		LCDstring("Hz  ", 4);

		LCDGotoXY(0,1);
		LCDstring("Note: ", 6);

		j = binSearch(tabHz, adc_val, MAX_TAB_HZ);
		t = j - (uint32_t)(j/12)*12;

		if(tabHz[j] == adc_val)
		{
			LCDstring(tabNote[t], 3);
			LCDstring("   ", 3);
		}
		else if( adc_val > tabHz[0] && adc_val < tabHz[MAX_TAB_HZ-1])
		{
			diff0 = tabHz[j] - adc_val;
			diff1 = adc_val - tabHz[j-1];
			if(diff1 > diff0)
			{
				LCDstring(tabBetweenNote[t], 6);
			}
			else
			{
				LCDstring(tabBetweenNote[t-1], 6);
			}
		}
	}
	return 0;
}

ISR(TIMER1_OVF_vect)
{
  phaccu=phaccu+tword_m;
  icnt=phaccu >> 24;
  OCR1A=pgm_read_byte_near(sine256 + icnt);
}
